$(document).ready(function(){
	// Navigation
	(function(){
		var hamburger = document.getElementById('hamburger'),
			closeNavigation = document.getElementById('navigation-close'),
			navigation = document.getElementById('navigation');

		hamburger.onclick = function(e){
			e.preventDefault();
			navigation.style.display = 'block';
			document.body.className += ' overflow-hidden';
		};
		closeNavigation.onclick = function(e){
			e.preventDefault();
			navigation.style.display = 'none';
			document.body.classList.remove('overflow-hidden');
		}

		document.onclick = function(e){
			if ( (!$(navigation).is(e.target) && $(navigation).has(e.target).length === 0 ) &&
				(!$(hamburger).is(e.target) && $(hamburger).has(e.target).length === 0 ) ) {
				if ( window.innerWidth <= 992 ) {
					navigation.style.display = 'none';
				}
			}
		};

		$(window).resize(function(){
			if ( window.innerWidth > 992 ) {
				$(navigation).attr('style','');
				document.body.classList.remove('overflow-hidden');
			}
		});

	}());

	// Add product elected
	$('#products').on('click','.to-elected', function(e){
		$(this).toggleClass('product__elected-selected');
	});

	// To top
	(function(){
		var top = $('.on-to-top');
		$(window).scroll(function(){
			checkToTop();
		});

		top.click(function(){
			$("html, body").animate({ scrollTop: 0 }, "slow");
		});
		
		checkToTop();

		function checkToTop() {
			if ( $('html,body').scrollTop() < 150 ) {
				top.addClass('hide');
			}
			else {
				top.removeClass('hide');
			}
		}
	}());

	(function(){

		var btn = $('.to-load-more'),
			file = 'products.json',
			page = 0,
			perPage = 6,
			orderBy = "price",
			checkedOrderBy = $('.sorting__radio:checked');

		btn.click(function(){
			loadJson(file);
		});

		$('.sorting__radio').on('click',function(){
			if ( $(this).val() == checkedOrderBy.val() ) return;
			orderBy = $(this).val();
			page = 0;
			$('#products').html("");
			$('.to-load-more').show();
			loadJson(file);
			checkedOrderBy = $(this);
		});

		loadJson(file);
		function loadJson(file){
			var xhr = new XMLHttpRequest();
			xhr.open('GET', file, true);
			xhr.send();
			xhr.onreadystatechange = function(){
				if ( xhr.readyState != 4 ) return;
				if (xhr.status == 200) {
					var jsonData = JSON.parse(xhr.responseText);

					var products = sorting(jsonData.products, orderBy);

					var products = products.slice((++page * perPage) - perPage, page*perPage);
					if ( !jsonData.products[page*perPage+1] ) {
						$('.to-load-more').hide();
					}
					renderHtml(products);
				}
			}
		}

		function sorting(data, field) {
			data.sort(compare);

			function compare(a,b) {
				if (a[field] < b[field])
					return -1;
				if (a[field] > b[field])
					return 1;
				return 0;
			}

			return data;
		}

		function renderHtml(data) {
			var htmlString = "";
			data.forEach(function(val, ind) {
				var elected = "";
				if ( val.elected ) {
					elected = "product__elected-selected";
				}
				var price = String(val.price).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				var sale = "";
				if ( val.sale ) {
					for(var i = 0; i < val.sale.length; i++) {
						sale += '<span>' + val.sale[i] + '</span>';
					}
				}
				htmlString += '<div class="product-wrap"><article class="product product-'+val.status[0]+'"><span class="product__elected '+elected+' to-elected"></span><div class="product__sale">'+sale+'</div><div class="product__content"><div class="product__img"><img src="'+val.image+'" alt=""></div><h2 class="product__name">'+val.name+'</h2><div class="product__prop"><p class="product__decor">'+val.decor+'</p><p class="product__prop-value"><span>'+val.area+' м<sup>2</sup></span><br />площадь</p><p class="product__prop-value"><span>'+val.floor+'</span><br />этаж</p></div><p class="product__price">'+price+'</p></div><div class="product__status"><p>'+val.status[1]+'</p></div></article></div>';
			});

			$('#products').append(htmlString);
		}

	}());

});

// Form validation (email)
function validation(th) {
	var email = document.getElementById('email').value,
		emailPattern = /[0-9a-z_-]+@[0-9a-z_-]+\.[a-z]{2,5}/i,
		msgError = '';
	if ( !emailPattern.test(email) ) {
		msgError += 'Не коректный email ';
	}

	if ( msgError ) {
		alert(msgError);
	}
	else {
		alert('Вы успешно подписались!');
		var inputs = $(th).find('input');
		inputs.val('');
	}
}
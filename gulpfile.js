var gulp 			= require('gulp'),
	rimRaf 			= require('rimraf'),
	sass 			= require('gulp-sass'),
	watch 			= require('gulp-watch'),
	rigger 			= require('gulp-rigger'),
	rename 			= require('gulp-rename'),
	uglify 			= require('gulp-uglify'),
	plumber		 	= require('gulp-plumber'),
	htmlminify 		= require("gulp-html-minify");
	sync  			= require('browser-sync'),
	imgMin 			= require('gulp-imagemin'),
	cssnano 		= require('gulp-cssnano'),
	preFixer	 	= require('gulp-autoprefixer'),
	reload 			= sync.reload;

var path = {

	dist: {
		html: 	'dist/',
		js: 	'dist/js/',
		css: 	'dist/css/',
		img: 	'dist/images/',
		fonts: 	'dist/fonts/',
	},
	src: {
		html: 	'src/*.html',
		js: 	'src/js/*.js',
		css: 	'src/scss/**/*.scss',
		img: 	'src/images/**/*',
		fonts: 	'src/fonts/**/*',
	},
	watch: {
		html: 	'src/**/*.html',
		js: 	'src/js/**/*.js',
		css: 	'src/scss/**/*.scss',
		img: 	'src/images/**/*',
		fonts: 	'src/fonts/**/*',
	},
	production: {
		html: 	'production/',
		js: 	'production/js/',
		css: 	'production/css/',
		img: 	'production/images/',
		fonts: 	'production/fonts/',
	},
	clear: {
		dist: 		'./dist',
		production: './production'
	}
};

gulp.task('html', function(){
	gulp.src(path.src.html)
		.pipe(rigger())
		.pipe(gulp.dest(path.dist.html))
		.pipe(sync.stream());
});

gulp.task('style', function(){
	gulp.src(path.src.css)
	.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
	.pipe(preFixer())
	.pipe(gulp.dest(path.dist.css))
	.pipe(reload({stream:true}));
});

gulp.task('js', function(){
	gulp.src(path.src.js)
	.pipe(rigger())
	.pipe(plumber())
	.pipe(gulp.dest(path.dist.js))
	.pipe(reload({stream:true}));
});

gulp.task('img', function(){
	gulp.src(path.src.img)
	.pipe(gulp.dest(path.dist.img))
	.pipe(reload({stream:true}));
});

gulp.task('fonts', function(){
	gulp.src(path.src.fonts)
	.pipe(gulp.dest(path.dist.fonts))
});

gulp.task('build', ['clear'], function(){
	gulp.start(['html','style', 'js', 'img', 'fonts']);
});

gulp.task('watch',['webserver'], function(){
	watch([path.watch.html], function(ev, callback) {
		gulp.start('html');
	});
	watch([path.watch.css], {readDelay: 100}, function(ev, callback) {
		gulp.start('style')
	});
	watch([path.watch.js], function(ev, callback) {
		gulp.start('js');
	});
	watch([path.watch.img], function(ev, callback) {
		gulp.start('img');
	});
	watch([path.watch.fonts], function(ev, callback) {
		gulp.start('fonts');
	});
});

gulp.task('webserver',['build'], function(){
	sync({
		server: {
		  baseDir: path.clear.dist
		},
		port: 8080,
		open: true,
		notify: false
	});
});

gulp.task('default', ['watch']);


gulp.task('imgmin', function(){
	gulp.src(path.src.img)
	.pipe(imgMin())
	.pipe(gulp.dest(path.dist.img));
});

gulp.task('clear', function(callback){
	rimRaf(path.clear.dist, callback);
});


// Build Production

gulp.task('production', ['prod_clear'],  function(callback){
	gulp.start(['prod_html','prod_style','prod_js','prod_img','prod_fonts']);
});

gulp.task('prod_html', function(){
	gulp.src(path.src.html)
		.pipe(rigger())
		.pipe(gulp.dest(path.production.html))
		.pipe(rename(function (path) {
			path.basename += "-min";
		}))
		.pipe(htmlminify())
		.pipe(gulp.dest(path.production.html))
});

gulp.task('prod_style', function(){
	gulp.src(path.src.css)
	.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
	.pipe(preFixer())
	.pipe(gulp.dest(path.production.css))
	.pipe(rename(function (path) {
		path.basename += "-min";
	}))
	.pipe(cssnano())
	.pipe(gulp.dest(path.production.css))
});

gulp.task('prod_js', function(){
	gulp.src(path.src.js)
	.pipe(rigger())
	.pipe(gulp.dest(path.production.js))
	.pipe(plumber())
	.pipe(uglify())
	.pipe(rename(function (path) {
		path.basename += "-min";
	}))
	.pipe(gulp.dest(path.production.js))
});

gulp.task('prod_img', function(){
	gulp.src(path.src.img)
	.pipe(imgMin())
	.pipe(gulp.dest(path.production.img))
});

gulp.task('prod_fonts', function(){
	gulp.src(path.src.fonts)
	.pipe(gulp.dest(path.production.fonts))
});

gulp.task('prod_clear', function(callback) {
	rimRaf(path.clear.production, callback);
});